import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  render() {
    return (
      <div className="container" s>
        <div className="row">
          {this.props.data.map((item) => {
            return (
              <div className="col-3">
                <ItemShoe
                  handleViewDetail={this.props.handleXemChiTiet}
                  handleAddToCart={this.props.handleAddToCart}
                  detail={item}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
