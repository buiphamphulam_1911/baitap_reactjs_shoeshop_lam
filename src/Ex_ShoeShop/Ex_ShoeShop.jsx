import React, { Component } from "react";
import { dataShoe } from "./data_shoe";
import Detail_Shoe from "./Detail_Shoe";
import GioHang from "./GioHang";
import ListShoe from "./ListShoe";
export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detailShoe: dataShoe[0],
    gioHang: [],
  };
  handleXemChiTiet = (idShoe) => {
    let index = this.state.shoeArr.findIndex((item) => {
      return item.id === idShoe;
    });
    index !== -1 &&
      this.setState({
        detailShoe: this.state.shoeArr[index],
      });
  };

  handleAddToCart = (shoe) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index == -1) {
      let spGioHang = { ...shoe, soLuong: 1 };
      cloneGioHang.push(spGioHang);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({
      gioHang: cloneGioHang,
    });
  };
  cong = (idShoe) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    cloneGioHang[index].soLuong++;
    this.setState({
      gioHang: cloneGioHang,
    });
  };
  tru = (idShoe) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    cloneGioHang[index].soLuong--;
    this.setState({
      gioHang: cloneGioHang,
    });
  };
  xoa = (idShoe) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    cloneGioHang[index].soLuong = 0;
    this.setState({
      gioHang: cloneGioHang,
    });
  };
  render() {
    console.log(this.state.gioHang.length);
    return (
      <div>
        <GioHang
          gioHang={this.state.gioHang}
          cong={this.cong}
          tru={this.tru}
          xoa={this.xoa}
        />
        <ListShoe
          data={this.state.shoeArr}
          handleXemChiTiet={this.handleXemChiTiet}
          handleAddToCart={this.handleAddToCart}
        />
        <Detail_Shoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
